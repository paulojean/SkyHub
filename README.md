# SkyHub

# Dependencies
- [Node](https://nodejs.org/)
- [Mongodb](https://www.mongodb.com/)

# Run
```
## ensuew mongod is runnning on another terminal
npm install
npm run start
```

using `curl` (or accessing on the broweser) you'll get the response in the following format
```json
[
  {
    "name":"b737_5.jpg",
    "sizes":{
       "large" : { "url":"http://localhost:300/images/large/b737_5.jpg" },
       "medium": { "url":"http://localhost:300/images/medium/b737_5.jpg" },
       "small" : { "url":"http://localhost:300/images/small/b737_5.jpg" }}
  }
]
```


I didn't managed to add tests  ¯\_(ツ)_/¯
