const sharp = require('sharp')

const imagesSizes = new Map([
  ['small', [320, 240]],
  ['medium', [384, 288]],
  ['large', [640, 480]] ])

module.exports = image => Promise.all(
    Array.from(imagesSizes.keys()).map(size => {
      const dimen = imagesSizes.get(size)
      return sharp(image.data)
        .resize(dimen[0], dimen[1])
        .toBuffer()
        .then(data => Object({ size, data }))}))
