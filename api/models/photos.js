const mongoose = require('mongoose')
const Schema = mongoose.Schema

const PhotoSchema  = new Schema({
  url: { type: String },
  image: {
    data: Buffer,
    contentType: String } })
PhotoSchema.methods.toJSON = function() {
  var size = this.toObject()
  delete size._id
  delete size._v
  return size }

const PhotosSchema = new Schema({
  name: { type: String, index: true },
  sizes: {
    small:    { type: PhotoSchema },
    medium:   { type: PhotoSchema },
    large:    { type: PhotoSchema } }})

PhotosSchema.statics.allWithoutBuffer = function allWithoutBuffer (callback) {
  return this.find({}, {
    _id: 0,
    'name': 1,
    'sizes.small.url': 1,
    'sizes.medium.url': 1,
    'sizes.large.url': 1, }, callback)}

module.exports = mongoose.model('Photos', PhotosSchema)
