const cron = require('node-cron')
const rp = require('request-promise')
const express = require('express')
const app = express()
const port = process.env.PORT || 3000
const mongoose = require('mongoose')
const Photos = require('./api/models/photos')
const resize = require('./api/utils/resize')
const BASE_URL = 'http://localhost:300'
const config = require('config')
mongoose.Promise = global.Promise
mongoose.connect(config.db_host)

Photos.remove({}, (err, data) => { })

const server = app.listen(port)

app.route('/')
  .get((req, res, next) => {
    Photos.allWithoutBuffer((err, images) => {
      if(err) {
        res.statusCode = 500
        res.send({'message':'Error retriving photos'})}
      else res.send(images) })})

app.route('/images/:size/:name')
  .get((req, res) => {
    const size = req.params.size
    const name = req.params.name
    Photos.findOne({ name }, (err, images) => {
      if(err) {
        res.statusCode = 500
        res.send({'message':`Error retreiving image: ${req.url}`}) }
      else if(!images){
        res.statusCode = 404
        res.send({'message':`Could not find the image: ${req.url}`}) }
      else {
        image = images.sizes[size]
        res.writeHead(200, {
          'Content-Type': 'image/jpg',
          'Content-Length': image.image.data.length })
        res.end(image.image.data) }})})

const getPhotosUrls = () => rp(config.source_photos)
  .then(data => JSON.parse(data).images)
  .then(images => images.map(i => i.url))

const retreivePhoto = url => rp(url, { encoding: null })
  .then(data => Object({ url, data }))

const savePhotoWithSizes = photo => resize(photo)
  .then(imagesResized => {
    const image = new Photos()
    const name = photoName(photo.url)
    image.name = name
    image.sizes = {}
    Object.values(imagesResized).forEach(img => {
      image.sizes[img.size] = Object({
        image: Object({ data: img.data, contentType: "image/jpg"}),
        url: `${BASE_URL}/images/${img.size}/${name}`,
        size: img.size })})
    image.save() })

const photoName = url => url.split('/').reverse()[0]

const getPhotosNotOnDb = urls => new Promise((resolve, reject) => {
  Photos.allWithoutBuffer((err, photos) => {
    const names = photos.map(p => p.name)
    const missingPhotos = urls.filter(url => !names.includes(photoName(url)))
    resolve(missingPhotos) })})

const fetchPhotos = () => getPhotosUrls()
  .then(getPhotosNotOnDb)
  .then(urls => Promise.all(urls.map(retreivePhoto)))
  .then(images => images.map(savePhotoWithSizes))

fetchPhotos()

cron.schedule('*/2 * * * *', fetchPhotos)

module.exports = server
console.log('Server started on: ' + port)
